<?php

class Tutor{
    public $UserId;
    public $IsBanned;
    public $Bio;
    public $OverallRating;
    public $Offerings;
    public $Phone;
    public $FirstName;
    public $LastName;
    public $Email;
    public $Year;
    public $Major;
    public $College;
    public $MessengerLink;

    private $TotalTimesRated;
    private $OverallRatingSum;
    function __construct()//Constructor from userId
    {

    }

    public static function MakeNewTutor($first,$last,$email,$password,$school,$major,$year,$classes,$messengerLink,$bio){
        //add in logic to add tutor to database, then return said tutor
        global $hash;
        global $mysqli;
        $password=crypt($password,$hash);
        $query="INSERT INTO Users (IsTutor,FirstName,LastName,Email,Major,messengerLink,Password,Year,College) VALUES (1,?,?,?,?,?,?,?,?);";
        $stmt = $mysqli->prepare($query);

        $stmt->bind_param('ssssssss', $first,$last,$email,$major,$messengerLink,$password,$year,$school);
        $stmt->execute();

        $UserId=$stmt->insert_id;
        $stmt->close();
	
        //Add tutor table stuff
        $query="INSERT INTO Tutors (isBanned,UserId, Bio) VALUES (0,?,?);";
        $stmt = $mysqli->prepare($query);
	$stmt->bind_param('is', $UserId,$bio);
        $stmt->execute();

        //Add offerings stuff
      	foreach ($classes as $classJson) {
	    if(!empty($classJson['class'])) {
		$ClassId = $classJson['class'];
		//Check if class exists
		$query="SELECT * FROM `Classes` WHERE `ClassId` = ?";
		$stmt = $mysqli->prepare($query);
		$stmt->bind_param('i', $ClassId);
    
		if(!$stmt){
		    printf("Query Prep Failed: %s\n", $mysqli->error);
		    exit;
		}
		$stmt->execute();
		$result = $stmt->get_result();
		if($result->num_rows == 0) {
		  //add class if it doesn't exist
		  $query="INSERT INTO Classes (OfficialIdentifier,OfficialName,InformalName,College) VALUES (\"Pending\",\"Pending\",?,?)";
		  $stmt = $mysqli->prepare($query);
    
		  $stmt->bind_param('ss', $classJson['class'],$classJson['school']);
		  $stmt->execute();
		  $ClassId=$stmt->insert_id;
		  $stmt->close();
		}
    
		$query="INSERT INTO TutorOfferings (Approved,Rate,Rating,NumberOfRatings,ClassId,UserId,Grade) VALUES (\"Pending\",10,0,0,?,?,?)";
		$stmt = $mysqli->prepare($query);
    
		$stmt->bind_param('iid', $ClassId,$UserId,$classJson['grade']);
		$stmt->execute();
		$stmt->close();
	    }
        }

        return array("Your application has been successfully received. You should expect to receive an email from us in the next 24 hours.", $UserId);
    }
    public static function FromId($id){
        $Tutor=new self();
        $Tutor->getFromId($id);
        return $Tutor;
    }
    protected function getFromId($id){
        $this->UserId=$id;
        global $mysqli;
        $stmt = $mysqli->prepare("SELECT * FROM `Tutors` JOIN `TutorOfferings` on `Tutors`.`UserId`=`TutorOfferings`.`UserId` JOIN `Classes` on `TutorOfferings`.`ClassId`=`Classes`.`ClassId`  Join `Users` on `Users`.`UserId`= `Tutors`.`UserId` Where `Tutors`.`UserId`= ? and `Tutors`.`IsBanned`=False");
        if(!$stmt){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
        }
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();

        $this->Offerings=array();
        $firstRun=true;
        while($row = $result->fetch_assoc()){
            if($firstRun){
                if(is_null($row["UserId"])){
                    break;//if UserId is null, that row doesn't exist
                }
                $this->Bio=$row["Bio"];
                $this->IsBanned=$row["IsBanned"];
                $this->Phone=$row["Phone"];
                $this->FirstName=$row["FirstName"];
                $this->LastName=$row["LastName"];
                $this->Email=$row["Email"];
                $this->Year=$row["Year"];
                $this->Major=$row["Major"];
                $this->College=$row["College"];
                $this->MessengerLink=$row["MessengerLink"];
                $firstRun=false;
            }

            $this->Offerings[] = Offering::FromRow($row);
        }
        $stmt->close();
    }

    public static function FromIdOnlyApproved($id){
        $Tutor=new self();
        $Tutor->getFromIdOnlyApproved($id);
        return $Tutor;
    }


    protected function getFromIdOnlyApproved($id){
        $this->UserId=$id;
        global $mysqli;
        $stmt = $mysqli->prepare("SELECT * FROM `Tutors` JOIN `TutorOfferings` on `Tutors`.`UserId`=`TutorOfferings`.`UserId` JOIN `Classes` on `TutorOfferings`.`ClassId`=`Classes`.`ClassId`  Join `Users` on `Users`.`UserId`= `Tutors`.`UserId` Where `Tutors`.`UserId`= ? and `Tutors`.`IsBanned`=False AND `TutorOfferings`.`Approved`=\"Approved\"");
        if(!$stmt){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
        }
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();

        $this->Offerings=array();
        $firstRun=true;
        while($row = $result->fetch_assoc()){
            if($firstRun){
                if(is_null($row["UserId"])){
                    break;//if UserId is null, that row doesn't exist
                }
                $this->Bio=$row["Bio"];
                $this->IsBanned=$row["IsBanned"];
                $this->Phone=$row["Phone"];
                $this->FirstName=$row["FirstName"];
                $this->LastName=$row["LastName"];
                $this->Email=$row["Email"];
                $this->Year=$row["Year"];
                $this->Major=$row["Major"];
                $this->College=$row["College"];
                $this->MessengerLink=$row["MessengerLink"];
                $firstRun=false;
            }

            $this->Offerings[] = Offering::FromRow($row);
        }
        $stmt->close();
    }

    public static function FromRow($row){
        $Tutor=new self();
        $Tutor->getFromRow($row);
        return $Tutor;
    }
    protected function getFromRow($row){
        $this->UserId=$row["UserId"];
        $this->Bio=$row["Bio"];
        $this->IsBanned=$row["IsBanned"];
        $this->OverallRating=$row["OverallRating"];
        $this->Phone=$row["Phone"];
        $this->FirstName=$row["FirstName"];
        $this->LastName=$row["LastName"];
        $this->Email=$row["Email"];
        $this->Year=$row["Year"];
        $this->Major=$row["Major"];
        $this->College=$row["College"];
        $this->MessengerLink=$row["MessengerLink"];
        $this->Offerings[] = Offering::FromRow($row);
    }
}

class Student {
    public $UserId;
    public $FirstName;
    public $LastName;
    public $Email;

    function __construct()//Constructor from userId
    {

    }

    public static function MakeNewStudent($first,$last,$email,$password=null,$school=null,$major=null,$year=null,$messengerLink=null){
	//add in logic to add student to database, then return said student
        global $hash;
        global $mysqli;
        $password=crypt($password,$hash);
        $query="INSERT INTO Users (IsTutor,isStudent,FirstName,LastName,Email,Major,messengerLink,Password,Year,College) VALUES (0,1,?,?,?,?,?,?,?,?);";
        $stmt = $mysqli->prepare($query);

        $stmt->bind_param('ssssssss', $first,$last,$email,$major,$messengerLink,$password,$year,$school);
        $stmt->execute();

        $UserId=$stmt->insert_id;
        $stmt->close();
	
	return array("Your account has been successfully created.", $UserId);
    }
    
    public static function FromId($id){
        $Student=new self();
        $Student->getFromId($id);
        return $Student;
    }
    protected function getFromId($id){
        global $mysqli;
        $stmt = $mysqli->prepare("SELECT UserId, FirstName, LastName, Email, isStudent FROM Users Where UserId = ?");
        if(!$stmt){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
        }
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();

        $row = $result->fetch_assoc();
	if(!is_null($row["UserId"])){
	    $this->FirstName=$row["FirstName"];
	    $this->LastName=$row["LastName"];
	    $this->Email=$row["Email"];
	    $this->UserId=$row["UserId"];
	}
      
        $stmt->close();
	
	//Update user if not registered as a student
	if(!$row["isStudent"]) {
	    $stmt = $mysqli->prepare("UPDATE Users SET IsStudent = '1' WHERE UserId = ?");
	    if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	    }
	    $stmt->bind_param('i', $id);
	    $stmt->execute();
	    $stmt->close();
	}
    }
}


class Offering{
    public $OfferingId;
    public $UserId;
    public $ClassId;
    public $Approved;
    public $Grade;
    public $Rate;
    public $Rating;
    public $NumberOfRatings;
    public $OfficialIdentifier;
    public $OfficialName;
    public $InformalName;
    
    function __construct() {
	
    }
    
    function getFromRow($OfferingRow)
    {
        $this->OfferingId=$OfferingRow["OfferingId"];
        $this->UserId=$OfferingRow["UserId"];
        $this->ClassId=$OfferingRow["ClassId"];
        $this->Approved=$OfferingRow["Approved"];
        $this->Grade=$OfferingRow["Grade"];
        $this->Rate=$OfferingRow["Rate"];
        $this->Rating=$OfferingRow["Rating"];
        $this->NumberOfRatings=$OfferingRow["NumberOfRatings"];
        $this->OfficialIdentifier=$OfferingRow["OfficialIdentifier"];
        $this->OfficialName=$OfferingRow["OfficialName"];
        $this->InformalName=$OfferingRow["InformalName"];
    }
    
    public static function FromRow($OfferingRow) {
        $Offering=new self();
        $Offering->getFromRow($OfferingRow);
        return $Offering;
    }
    
    public static function FromClassAndUserIds($classId, $userId){
        $Offering=new self();
        $Offering->getFromClassAndUserIds($classId, $userId);
        return $Offering;
    }
    protected function getFromClassAndUserIds($classId, $userId){
        global $mysqli;
        $stmt = $mysqli->prepare("SELECT * FROM `TutorOfferings` JOIN `Classes` on `TutorOfferings`.`ClassId` = `Classes`.`ClassId` WHERE `TutorOfferings`.`ClassId` = ? AND `TutorOfferings`.`UserId` = ?");
        if(!$stmt){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
        }
        $stmt->bind_param('ii', $classId, $userId);
        $stmt->execute();
        $result = $stmt->get_result();
	$row = $result->fetch_assoc();
	$this->getFromRow($row);
        $stmt->close();
    }
}
class _Class{ //Can't call the class class class because class is reserved for classes
    public $ClassId;
    public $OfficialIdentifier;
    public $OfficialName;
    public $InformalName;
    public $College;
    function getFromRow($row){
        $this->ClassId=$row["ClassId"];
        $this->OfficialIdentifier=$row["OfficialIdentifier"];
        $this->OfficialName=$row["OfficialName"];
        $this->InformalName=$row["InformalName"];
        $this->College=$row["College"];
    }
    public static function FromRow($row){
        $Class=new self();
        $Class->getFromRow($row);
        return $Class;
    }

}

class Review{
    public $ReviewId;
    public $CreateDate;
    public $AuthorId;
    public $TutorId;
    public $Review;
    public $AuthorName;

    function __construct()//Constructor from userId
    {

    }
    public static function MakeNewReview($authorId,$tutorID,$review,$availability,$knowledge, $ability, $timeliness, $productivity, $receptivity){
        global $hash;
        global $mysqli;

        $query="INSERT INTO Reviews (AuthorId, TutorId, Review, Availability, Knowledge, Ability, Timeliness, Productivity, Receptivity) VALUES (?,?,?,?,?,?,?,?,?);";
        $stmt = $mysqli->prepare($query);

        $stmt->bind_param('sssiiiiii', $authorId,$tutorID,$review,$availability,$knowledge,$ability,$timeliness,$productivity,$receptivity);
        $stmt->execute();

        $ReviewId=$stmt->insert_id;
        $stmt->close();


        return array("Review has been successfully added!", $ReviewId);
    }

    public static function FromRow($row){
        $Review=new self();
        $Review->getFromRow($row);
        return $Review;
    }
    protected function getFromRow($row){
        $this->ReviewId=$row["ReviewId"];
        $this->AuthorId=$row["AuthorId"];
        $this->TutorId=$row["TutorId"];
        $this->Review=$row["Review"];
        $this->CreateDate=$row["CreateDate"];
        $this->AuthorName=$row["FirstName"];
    }
    
    public static function getNumberOfReviewsForTutorByStudent($Tutor, $Student) {
	global $mysqli;
	$stmt = $mysqli->prepare("SELECT COUNT(*) FROM Reviews WHERE AuthorId = ? AND TutorId = ?");
	if(!$stmt) {
	    printf("Query Prep Failed: %s\n", $mysqli->error);
	    exit;
	}
	$stmt->bind_param('ii', $Student->UserId, $Tutor->UserId);
        $stmt->execute();
        $stmt->bind_result($count);
	$stmt->fetch();
        $stmt->close();
	return $count;
    }

}

class Session {
    public $SessionId;
    public $Offering;
    public $Tutor;
    public $Student;
    public $Date;
    public $Rate;
    public $Duration;
    public $TotalAmountOwed;
    
    function __construct()
    {

    }
    
     public static function MakeNewSession($classId,$tutorId,$studentId,$date,$rate,$duration,$totalAmountOwed){
        //get offering id
	$offeringId = Offering::FromClassAndUserIds($classId, $tutorId)->OfferingId;
	
	global $hash;
        global $mysqli;

        $query="INSERT INTO Sessions (OfferingId, StudentUserId, Date, Rate, Duration, TotalAmountOwed) VALUES (?,?,?,?,?,?);";
        $stmt = $mysqli->prepare($query);

        $stmt->bind_param('iisidd', $offeringId,$studentId,$date,$rate,$duration,$totalAmountOwed);
        $stmt->execute();

        $SessionId=$stmt->insert_id;
        $stmt->close();

        return array("The Session Form has been successfully submitted.", $SessionId);
    }
    
    public static function FromId($id){
	$Session = new self();
	$Session->getFromId($id);
	return $Session;
    }
    protected function getFromId($id){
        global $mysqli;
        $stmt = $mysqli->prepare("SELECT * FROM `Sessions` JOIN `TutorOfferings` on `Sessions`.`OfferingId`=`TutorOfferings`.`OfferingId` JOIN `Classes` on `TutorOfferings`.`ClassId` = `Classes`.`ClassId` Join `Users` on `Users`.`UserId`= `TutorOfferings`.`UserId` Join `Tutors` on `Users`.`UserId`=`Tutors`.`UserId` WHERE `Sessions`.`SessionId` = ?");
        if(!$stmt){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
        }
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $row = $result->fetch_assoc();
	
	if(!is_null($row["SessionId"])){
	    $this->SessionId=$row["SessionId"];
	    $this->Offering=Offering::FromRow($row);
	    $this->Tutor=Tutor::FromRow($row);
	    $this->Student=Student::FromId($row["StudentUserId"]);
	    $this->Date=date_create($row["Date"]);
	    $this->Rate=$row["Rate"];
	    $this->Duration=$row["Duration"];
	    $this->TotalAmountOwed=$row["TotalAmountOwed"];
	}

        $stmt->close();
    }
    
    public function getDay(){
	return date_format($this->Date, "l");
    }
    
    public function getSessionNumber($forSpecificClass=false) {
	global $mysqli;
	if($forSpecificClass) {
	    $stmt = $mysqli->prepare("SELECT COUNT(*) FROM Sessions WHERE StudentUserId = ? AND OfferingId = ?");
	    if(!$stmt) {
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	    }
	    $stmt->bind_param('ii', $this->Student->UserId, $this->Offering->OfferingId);
	}
	else {
	    $stmt = $mysqli->prepare("SELECT COUNT(*) FROM Sessions WHERE StudentUserId = ?");
	    if(!$stmt) {
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	    }
	    $stmt->bind_param('i', $this->Student->UserId);
	}
        $stmt->execute();
        $stmt->bind_result($count);
	$stmt->fetch();
        $stmt->close();
	return $count;
    }
}


?>
