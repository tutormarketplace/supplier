<?php

/**
 * @param null $ClassId
 */
function getAllTutors($ClassId=null){
    global $mysqli;
    $SQLQuery="SELECT * FROM `Tutors` JOIN `TutorOfferings` on `Tutors`.`UserId`=`TutorOfferings`.`UserId`  JOIN `Classes` on `TutorOfferings`.`ClassId`=`Classes`.`ClassId`  Join `Users` on `Users`.`UserId`= `Tutors`.`UserId` Where `Tutors`.`IsBanned`=False AND `TutorOfferings`.`Approved`=\"Approved\"";
    if(!is_null($ClassId)){
        $SQLQuery.=" and `TutorOfferings`.`ClassId` = ?";
        $SQLQuery.=" order by `TutorOfferings`.`Rating` DESC";
    }
    else{
        $SQLQuery.=" order by `Tutors`.`OverallRating` DESC";
    }

    $stmt = $mysqli->prepare($SQLQuery);

    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    if(!is_null($ClassId)){
        $stmt->bind_param('i', $ClassId);
    }
    $stmt->execute();
    $result = $stmt->get_result();
    $TutorsMap=array();
    while($row = $result->fetch_assoc()){
        $id=$row["UserId"];
        if(!isset($TutorsMap[$id])){
            $TutorsMap[$id]=Tutor::FromRow($row);
        }
        else {
            $TutorsMap[$id]->Offerings[] = Offering::FromRow($row);
        }
    }
    $Tutors=array();
    foreach($TutorsMap as $tutor){
        $Tutors[]=$tutor;
    }

    return $Tutors;
}

function getAllReviews($TutorId=null){
    global $mysqli;
    $SQLQuery= "SELECT * FROM `Reviews` JOIN  `Users` on `Users`.`UserId` = `Reviews`.`AuthorId` where `TutorId` = ?";
    //OLD QUERY: SELECT * FROM `Reviews` Where `TutorId`= ? JOIN `Users` on `Users`.`UserId` = ?
    
    $stmt = $mysqli->prepare($SQLQuery);

    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    if(!is_null($TutorId)){
        $stmt->bind_param('i', $TutorId);
    }
    $stmt->execute();
    $result = $stmt->get_result();

    $ReviewsMap=array();
    
    $Reviews=array();
    while($row = $result->fetch_assoc()){
        $Reviews[]=Review::FromRow($row);
    }
    
    return $Reviews;
}

function getAllClasses(){
    global $mysqli;
    $SQLQuery="SELECT * FROM `Classes`";
    $stmt = $mysqli->prepare($SQLQuery);

    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->execute();
    $result = $stmt->get_result();
    $Classes=array();
    while($row = $result->fetch_assoc()){
        $Classes[]=_Class::FromRow($row);
    }

    return $Classes;
}

function computeRating($TutorId=null) {
   $Reviews = getAllReviews($TutorId);
   foreach ($Reviews as $review) {
     var_dump($review);
   }
}
?>
